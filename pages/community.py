from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User

import mysql.connector




@app.route('/community', methods=['GET'])
def community():
    cnx = mysql.connector.connect(user='root', password='root',
                                  host='localhost',
                                  database='ohm_assessment')

    cursor = cnx.cursor()
    query = ("SELECT user.user_id, user.display_name, user.tier, user.point_balance, rel_user.attribute\
                FROM\
                 user\
                 LEFT JOIN\
                 rel_user\
                 ON user.user_id = rel_user.user_id\
                 ORDER BY user.signup_date DESC\
                 LIMIT 5")
    cursor.execute(query)
    test = []
    for (user, name, tier, points, location) in cursor:
        test.append({
            'user': user,
            'name': name,
            'tier': tier,
            'points': points,
            'location': location,
            'phonenumber': []
        })

    query2 = ("SELECT user_id, attribute\
                FROM\
                rel_user_multi")
    cursor.execute(query2)
    phones = {}

    for (user, phone) in cursor:
        if user not in phones:
            phones[user] = []
        phones[user].append(phone)
    print phones

    cnx.close()


    return render_template("community.html", users=test, phones=phones)
