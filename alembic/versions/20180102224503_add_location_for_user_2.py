"""add location for user 2

Revision ID: 28548a14f8cd
Revises: 5af1d32bfc32
Create Date: 2018-01-02 22:45:03.050301

"""

# revision identifiers, used by Alembic.
revision = '28548a14f8cd'
down_revision = '5af1d32bfc32'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES
            (2, 'LOCATION', 'USA')
    ''')

def downgrade():
    pass
