"""change tier for user 3 to silver

Revision ID: 269b50ab9d85
Revises: 28548a14f8cd
Create Date: 2018-01-02 22:48:05.130634

"""

# revision identifiers, used by Alembic.
revision = '269b50ab9d85'
down_revision = '28548a14f8cd'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user
        SET tier = 'Silver'
        WHERE user_id=3
    ''')

def downgrade():
    pass
