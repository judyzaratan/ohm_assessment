"""increase point_balance for user1 to 1000

Revision ID: 5af1d32bfc32
Revises: 00000000
Create Date: 2018-01-02 22:05:48.755705

"""

# revision identifiers, used by Alembic.
revision = '5af1d32bfc32'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user
        SET point_balance = 1000
        WHERE user_id=1
    ''')

def downgrade():
    pass
